# Tufte


Tufte is an elegant and clean theme for [Hugo](http://gohugo.io/).

## Installation

```
mkdir themes
cd themes
git clone https://bitbucket.org/kkaisare/hugo-theme-tufte.git tufte
```

See the [official docs](http://gohugo.io/themes/installing) for more information.

## Build your site

```
hugo server -t tufte
```


## License

Open sourced under [MIT license](https://bitbucket.org/kkaisare/hugo-theme-tufte/src/700e791f0aff1fb867eb09694aa4dfd40d27a4a0/LICENSE.md?at=master&fileviewer=file-view-default).
